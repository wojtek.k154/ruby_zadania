def divisor_summation(n)
  sum = (1..n/2).find_all { |item| n%item==0 }.reduce(:+)
end

describe :divisor_summation do 
  it 'should return 6 for 25' do
  	expect( divisor_summation(25) ).to eq 6
  end
  it 'should return 117 for 100' do
  	expect( divisor_summation(100) ).to eq 117
  end
  it 'should return 143 for 200000' do
  	expect( divisor_summation(200000) ).to eq 296062
  end
  it 'should return 22 for 20' do
  	expect( divisor_summation(20) ).to eq 22
  end
end