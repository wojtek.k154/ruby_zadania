require 'benchmark'
include Benchmark

def reverse(number)
  temp = 0
  div = 1
  while number > 0 do 
    temp = temp*10
    temp = temp + (number % 10)
    number = number/10
  end
  return temp
end

def sum_rev(n1, n2)
  temp = reverse(n1) + reverse(n2)
  reverse(temp)
end

Benchmark.bmbm do |x|
  x.report("Benchmark 1") do
    100000.times do
      sum_rev(5264, 2896)
    end
  end
end

Benchmark.bmbm do |x|
  x.report("Benchmark 2") do
    100000.times do
      temp = 5264.to_s.reverse.to_i + 2896.to_s.reverse.to_i
      temp.to_s.reverse.to_i
    end
  end
end

describe :reverse do
 it 'return 321 for 123' do
 	expect( reverse(123) ).to eq 321
 end
 it 'return 568 for 865' do
 	expect( reverse(865) ).to eq 568
 end
 it 'return 8295 for 5928' do
 	expect( reverse(5928) ).to eq 8295 
 end
 it 'return 7469 for 9647' do
 	expect( reverse(9647) ).to eq 7469
 end
end

describe :sum_rev do
  it 'return 25 for 12 and 13' do 
  	expect( sum_rev(12,13) ).to eq 25
  end
  it 'return 1998 for 4358 and 754' do 
  	expect( sum_rev(4358,754) ).to eq 1998
  end
  it 'return 11 for 24 and 86' do 
  	expect( sum_rev(24,86) ).to eq 11
  end
end

